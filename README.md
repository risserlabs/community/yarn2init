# yarn2init

> initialize yarn2 projects

# Usage

```
mkdir -p my-new-project
cd my-new-project
yarn2init
```

# Install

```sh
./mkpm install
```

# Uninstall

```sh
./mkpm uninstall
```
