#!/bin/sh

if [ ! -d .git ]; then
    echo 'this command must be run at the root of a git repo' 1>&2
    exit 1
fi

if [ -f .yarnrc.yml ]; then
    echo 'yarn2 already setup for this project' 1>&2
    exit 1
fi
[ -f package.json ] && true || yarn init
yarn set version berry
yarn set version 3.x
(export PKG=$(node -e \
    "let p=require('./package.json');p.packageManager='yarn@3.1.0';console.log(JSON.stringify(p,null,2))") && \
    printf '%s\n' "$PKG" > package.json)
cat <<EOF >> .yarnrc.yml
enableTelemetry: false

logFilters:
  - code: YN0002
    level: discard
  - code: YN0060
    level: discard
  - code: YN0006
    level: discard
  - code: YN0076
    level: discard
  - code: YN0013
    level: discard

nodeLinker: node-modules
EOF
yarn plugin import workspace-tools
yarn plugin import plugin-interactive-tools
yarn plugin import plugin-typescript
yarn plugin import plugin-version
yarn
if git lfs --version >/dev/null; then
    git lfs track yarn.lock .pnp.cjs '.yarn/cache/**' '.yarn/releases/**' '.yarn/plugins/**'
else
    echo 'ERROR: please install git-lfs and run the following command'
    printf "\n    git lfs track yarn.lock .pnp.cjs '.yarn/cache/**' '.yarn/releases/**' '.yarn/plugins/**'\n\n"
    exit 1
fi
